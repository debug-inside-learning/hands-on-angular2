import { Component } from "angular2/core";
import { AuthorService } from "./author.service";

@Component({
    selector:'authors',
    template:`
    <h1>Authors</h1>
    <h3>{{title}}</h3>
    <ul>
    <li *ngFor="#author of authors">
    {{author}}
    </li>
    </ul>
    `,
    providers:[AuthorService]
})

export class AuthorsComponent
{
   title:string="Title of the Author page";
   authors;

   constructor(authorService: AuthorService)
   {
this.authors = authorService.getAuthors();
   }
}